#include "init.h"

#define CPU_F 8000000

static void SetSysClockToHSILP(void);
void NVIC_Configuration(void);
void RTC_Configuration(void);
void gpio_init(void);
void i2c_setup(void);

void init_all(void){
    
   // DBGMCU_Config(DBGMCU_STANDBY, ENABLE);
    
    gpio_init();
    SetSysClockToHSILP();
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE); 
    SysTick_Config(CPU_F/1000);
    RTC_Configuration();
    NVIC_Configuration();
    i2c_setup();
    ds3231_init();
    PWR_WakeUpPinCmd(ENABLE);
}

static void SetSysClockToHSILP(void)
{    
    RCC_DeInit();
    
    /* HCLK = SYSCLK */
    RCC_HCLKConfig(RCC_SYSCLK_Div1);
    /* PCLK2 = HCLK */
    RCC_PCLK2Config(RCC_HCLK_Div1); 
    /* PCLK1 = HCLK */
    RCC_PCLK1Config(RCC_HCLK_Div1);
}


void NVIC_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;

  /* Configure one bit for preemption priority */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

  /* Enable the RTC Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}


void RTC_Configuration(void)
{       
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);

    /* Allow access to BKP Domain */
    PWR_BackupAccessCmd(ENABLE);

    if(PWR_GetFlagStatus(PWR_FLAG_SB) != RESET)
    {
    /* Clear StandBy flag */
    PWR_ClearFlag(PWR_FLAG_SB);
    }
    else {   
      BKP_DeInit();
        
      RCC_LSICmd(ENABLE);
      while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
      {}

      /* Select LSE as RTC Clock Source */
      RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);

      /* Enable RTC Clock */
      RCC_RTCCLKCmd(ENABLE);

      /* Wait for RTC registers synchronization */
      RTC_WaitForSynchro();
      RTC_WaitForLastTask();

      /* Set RTC prescaler: set RTC period to 1sec */
      // For LSE: prescaler = RTCCLK/RTC period = 32768Hz/1Hz = 32768
      // For LSI: prescaler = RTCCLK/RTC period = 40000Hz/1Hz = 40000 
      RTC_SetPrescaler(39999);

      /* Wait until last write operation on RTC registers has finished */
      RTC_WaitForLastTask();
    }
  
}


void gpio_init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

	/* Turn on GPIO for power-control pin connected to FET's gate */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	/* Configure I/O for Power FET */
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10 | GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

    GPIO_SetBits(GPIOB, GPIO_Pin_10);
    
    /* Turn on GPIO for power-control pin connected to FET's gate */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	/* Configure I/O for Power FET */
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IPD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}


void i2c_setup(void)
{
   ds3231_init();
}

