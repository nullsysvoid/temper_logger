#ifndef __INCLUDE_H
#define __INCLUDE_H

#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_rtc.h"
#include "stm32f10x_bkp.h"
#include "stm32f10x_pwr.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_dma.h"
#include "misc.h"

#include <time.h>
#include <stdio.h>
#include "ff.h"
#include "diskio.h" 
#include <string.h>
#include "init.h"
#include "onewire.h"
#include "ds18b20.h"
#include "rtc_time.h"
#include "error_sign.h"
#include "i2c_hw.h"
#include "ds3231.h"

#endif 

