#include "include.h"

static void vRTCConvert(struct tm *rtc_now);

struct tm get_curtime(void)
{
    time_t rawtime = RTC_GetCounter();
    
    struct tm *info = localtime(&rawtime);
    vRTCConvert(info);
    return *info;
}

static void vRTCConvert(struct tm *rtc_now)
{
    rtc_now->tm_year += 1900;
    rtc_now->tm_mon++;
}   

void vRTCSetDT(struct tm *rtc_now)
{
  struct tm tim;
   
  tim.tm_year = rtc_now->tm_year - 1900;
  tim.tm_mon = rtc_now->tm_mon - 1;
  tim.tm_mday = rtc_now->tm_mday;
  tim.tm_hour = rtc_now->tm_hour;
  tim.tm_min = rtc_now->tm_min;
  tim.tm_sec = rtc_now->tm_sec;
  tim.tm_isdst = 0;
  
  RTC_SetCounter(mktime ( &tim ));
}

