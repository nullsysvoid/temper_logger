#include "ds3231.h"
#include "stm32f10x_i2c.h"
#include "i2c_hw.h"

//I2C address
#define DS3231_I2CADDR       (0x68)
#define DS3231_I2C           (I2C1)

static uint8_t ds3231_read(uint8_t reg, uint8_t *data_buf, uint8_t data_size)
{
    for(int i = 0; i < data_size; i++) { 
        i2c_write(DS3231_I2CADDR, reg + i);
        i2c_read(DS3231_I2CADDR, &data_buf[i]);
    }
	return 0;
}

static void ds3231_write(uint8_t reg, uint8_t *data_buf, uint8_t data_size)
{
    uint8_t data[16];
    
    data[0] = reg;
    memcpy(&data[1], data_buf, data_size);
    i2c_writeBuf(DS3231_I2CADDR, data, (data_size+1));
}

static uint8_t bcd2dec(uint8_t b)
{
  return ((b/16 * 10) + (b % 16));
}


void ds3231_init(void)
{
    i2c_init();  
}

uint8_t dec2bcd(uint8_t d)
{
  return ((d/10 * 16) + (d % 10));
}

void ds3231_get_time(struct tm* ds3231_tm)
{
	uint8_t rtc[9];

	ds3231_read(0, rtc, 9);

	ds3231_tm->tm_sec = bcd2dec(rtc[0] & 0x7f);    //sec
	ds3231_tm->tm_min = bcd2dec(rtc[1] & 0x7f);    //min
	ds3231_tm->tm_hour = bcd2dec(rtc[2] & 0x3f);   //hour

	ds3231_tm->tm_wday = bcd2dec(rtc[3] & 0x7);   //week day

	ds3231_tm->tm_mday = bcd2dec(rtc[4] & 0x3f);   //month day
	ds3231_tm->tm_mon = bcd2dec(rtc[5] & 0x1F);    //month

	uint8_t century = ((rtc[5] & 0x80) >> 7);

	ds3231_tm->tm_year = bcd2dec(rtc[6]);         //year 0-99
	if(century == 1) {
		ds3231_tm->tm_year += 2000;
	}
    else {
        ds3231_tm->tm_year += 1900;
    }
}

void ds3231_set_time(struct tm* ds3231_tm)
{
	uint8_t send_buf[8];

	send_buf[0] = dec2bcd(ds3231_tm->tm_sec);
	send_buf[1] = dec2bcd(ds3231_tm->tm_min);
	send_buf[2] = dec2bcd(ds3231_tm->tm_hour);
    
    send_buf[3] = dec2bcd(ds3231_tm->tm_wday);
	send_buf[4] = dec2bcd(ds3231_tm->tm_mday);
    send_buf[5] = dec2bcd(ds3231_tm->tm_mon);
    
	if(ds3231_tm->tm_year > 2000) {
        send_buf[6] = dec2bcd(ds3231_tm->tm_year - 2000);
        send_buf[5] |= (1 << 7);
	}
    else {
        
        send_buf[6] = dec2bcd(ds3231_tm->tm_year - 1900);
    }
	ds3231_write(0, send_buf, 7);
}



