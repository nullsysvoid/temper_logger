#ifndef _I2CHW_H
#define _I2CHW_H

#include "include.h"

void i2c_init(void);
void i2c_read(uint8_t address, uint8_t* data);
void i2c_write(uint8_t address, uint8_t data);
void i2c_writeBuf(uint8_t address, uint8_t *data, uint8_t size);

#endif 

