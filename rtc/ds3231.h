#ifndef DS3231_H
#define DS3231_H

#include <stdint.h>
#include <time.h>

void ds3231_init(void);
void ds3231_get_time(struct tm* ds3231_tm);
void ds3231_set_time(struct tm* ds3231_tm);

#endif
