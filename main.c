#include "include.h"

#define GLOBAL_TIMEOUT_SEC   10
#define SENSORS_TIMEOUT_MSEC   160



uint8_t buffer[512];
uint8_t sensors;
struct tm rtc_now;

uint8_t sensors_timeout;
uint8_t error_timeout;

uint16_t i2c_timeout = 500;
uint16_t i2c_status = 0;

void hadnler_100hz(void) {
    if(sensors_timeout > 0) {
        sensors_timeout--;
    }
    if(error_timeout > 0) {
        error_timeout--;
    }
    if(i2c_timeout > 0)  {
        i2c_timeout--;
    }
    else {
        if(i2c_status == 0) {
            error_Handle(2);
        }
    }
}

void SysTick_Handler(void)
{			
	static int cnt;
	cnt++;	
	if(cnt%10 == 0) {          //100hz handler
        disk_timerproc();    
        hadnler_100hz();
    }
}

void RTC_IRQHandler(void)
{
   if (RTC_GetITStatus(RTC_IT_SEC) != RESET)
   {
      RTC_ClearITPendingBit(RTC_IT_SEC);
      RTC_WaitForLastTask();
   }
}

int main(void)
{	
	FATFS f_s;
	FIL file;
	UINT br, bw;
    DIR directory;
    i2c_timeout = 500;

    //temp directory/file name
    char path[32];    
    
    //low level init peripheral
    i2c_status = 0;
    init_all();

    //blink
    LED1_ON();
    delay_ms(10);    
    LED1_OFF();
    
    //init temper sensors
    sensors = ds18b20_init();
    if(sensors == 0)  {
        error_Handle(0);
    }

    //start convert for all sensors
    ds18b20_start_convert();
    sensors_timeout = SENSORS_TIMEOUT_MSEC;

    //check sd card presence
	if(disk_initialize(0) != 0) {
        error_Handle(1);  //error
    }
    
    //mount fatfs
	if(f_mount(0,&f_s) != FR_OK) {
        error_Handle(1); //error
    }  
    
    //set date/time if need
    if(f_open(&file, "date.ini", FA_OPEN_EXISTING | FA_READ) == FR_OK) {
        f_read(&file, buffer, 128, &br);
        sscanf((char *)buffer, "%2d:%2d:%4d %2d:%2d:%2d", &rtc_now.tm_mday, &rtc_now.tm_mon, &rtc_now.tm_year, 
                                            &rtc_now.tm_hour, &rtc_now.tm_min, &rtc_now.tm_sec );
        vRTCSetDT(&rtc_now);
        ds3231_set_time(&rtc_now);

        f_unlink("date.ini");
    }
    else {
        //get current time
        ds3231_get_time(&rtc_now);
        vRTCSetDT(&rtc_now);
    }
    i2c_status = 1;
    
    //create dir name with date/time
    memset(path, 0, sizeof(path));
    sprintf(path, "%.4u%.2u%.2u", rtc_now.tm_year, rtc_now.tm_mon, rtc_now.tm_mday);
	
    //check dir
    if(f_opendir(&directory, path) == FR_NO_PATH) {
        f_mkdir(path);    
    }
    
    //create file name for current time
    char file_path[32];
    memset(file_path, 0, sizeof(path));
    memset(buffer, 0, sizeof(buffer));   
    sprintf(file_path, "%s/%.2u%.2u%.2u", path, rtc_now.tm_hour, rtc_now.tm_min, rtc_now.tm_sec);
    
    //wait for temper convert
    while (sensors_timeout > 0) {   //wait 1000ms
        __nop();
    }
    
    //get temper
    float temper;
    uint16_t buf_pos = 0;
    
    for(int i = 0; i < sensors; i++) {
        uint8_t tbuffer[16];
        temper = ds18b20_get_temp(i);             //get t from sensors
        sprintf((char *)tbuffer, "t[%d]=%.1f ", i, temper);
        uint8_t data_len = strlen((char *)tbuffer);
        memcpy(&buffer[buf_pos], tbuffer, data_len);
        buf_pos += data_len;
    } 
    buffer[buf_pos++] = 0x0D;
    
    //create file
    if(f_open(&file, file_path, FA_OPEN_EXISTING | FA_WRITE) == FR_NO_FILE) {
        f_open(&file, file_path, FA_CREATE_NEW | FA_WRITE);
    }
    f_write(&file, buffer, buf_pos, &bw);
    
    f_sync(&file);
    f_close(&file);
    
    if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0) == 1) {
        LED1_ON();
        while(1);
    }

    
    RTC_SetAlarm(RTC_GetCounter()+ GLOBAL_TIMEOUT_SEC);
    
    /* Wait until last write operation on RTC registers has finished */
    RTC_WaitForLastTask();
    
    PWR_EnterSTANDBYMode();
	while(1);	
} 			

