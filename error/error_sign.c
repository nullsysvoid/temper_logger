#include "include.h"

void error_signal(uint8_t loops, uint8_t repeats);

void delay_ms(uint32_t ms)
{
    volatile uint32_t nCount; 
    RCC_ClocksTypeDef RCC_Clocks; 
    RCC_GetClocksFreq (&RCC_Clocks);
     
    nCount=(RCC_Clocks.HCLK_Frequency/10000)*ms;
    for (; nCount!=0; nCount--);
}



void error_Handle(uint8_t err_code) {
    __disable_irq();
    while(1){
        error_signal(err_code+1, 1);
    }   
}

void error_signal(uint8_t loops, uint8_t repeats)
{
    LED1_OFF();
    for(int j = 0; j < repeats; j++) {
        for(int i = 0; i < loops; i++) {
            LED1_ON();
            delay_ms(150);
            LED1_OFF();
            delay_ms(150);
        }
        delay_ms(800);
    }
}



