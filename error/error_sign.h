#ifndef _ERROR_SIGN_H
#define _ERROR_SIGN_H

#include "include.h"

#define LED1_OFF()   GPIO_ResetBits(GPIOA, GPIO_Pin_2)  
#define LED1_ON()    GPIO_SetBits(GPIOA, GPIO_Pin_2)

void error_Handle(uint8_t err_code);
struct tm get_curtime(void);
void delay_ms(uint32_t ms);

#endif 

